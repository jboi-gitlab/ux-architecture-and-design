# UX-Architecture and Definition
As Architects, we all know how to define a system using UML and many other tools. We know how to follow processes and we have a number of frameworks like TOGAF available to design and define a system.

Except for the definition of the user interface.

So, this repository describes an approach to fill the gap: As an Architect, I'd like to define User Interface and User Experience, such that a system is ready to be implemented and consistently being used.

There're three proof-points to show, that the UX-Definition did a good job.

- When a User uses the system, he has at any time all necessary information visualized and at hand to fulfill a task in a consistent manner.
- When a back-end-developer has to implement the backend-API, he can derive all information-grouping and calling-sequences from the UX-Definition.
- When a UI-Desginer wants to design, he can derive what information needs to be made available (visual, audio, etc.) at any point of system usage.

This means, we're NOT talking about how to make an App simple and intuitive (and nice) and we're NOT defining the APIs to get or set all information, that the App is working with. We just create the part in between.

Or, as one would say:
> Weeks of programming can save you hours of planning.

## How to UX-Definition
1. Overview of what to do and how it makes live easier
2. A very simple example
3. Information-items, navigation-items, functions and groups
4. Deep dive into the process
    - Gather information-items
    - The users tasks and groupings
    - What functions need a task
    - Where does a user need to go to?
5. Make it good or even better: Some rules and measures

## Roadmap
- At the moment, it's all about describing the methodology.
- Further steps will be to automatically create a diagram and a measure along the given rules.
- A Copilot to create a UX-Defimnition from an existing App or a prototype or some API-Defintions should be enough for the current planings.

## Contributing
Let me establish the first idea and base rules. After that contributing is highly welcome. Whenever you know how to build a Copilot or automatically create diagrams and measures, leave me a message.

## Authors and acknowledgment
At the moment, it's me. Hope this changes some time ahead.

## License
This work is under MIT licence. See [license](LICENSE.md) for more information.

## Project status
Full of energy and just started. :-)
